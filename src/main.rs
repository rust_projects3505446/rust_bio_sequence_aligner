use bio::{
    alignment::{pairwise::*},
    io::fasta::Reader,
    scores::blosum62
};

use std::{fs::File,io::prelude::*,time::Instant};
fn main (){
    let start = Instant::now();
    let records = (Reader::from_file("../shrinked_sclerot.fasta")
        .expect("Error opening file"))
        .records().collect::<Vec<_>>();
    
    records.iter()
        .map(|s_result|s_result.as_ref().unwrap())
        .for_each(|s_result| {
                records
                    .iter()
                    .map(|t_result|t_result.as_ref().unwrap())
                    .filter(|t_result| s_result.id() != t_result.id())
                    .for_each(|t_result|{
                        println!(
                            "{}", Aligner::with_capacity(
                                s_result.seq().len(),
                                t_result.seq().len(), 
                                -5, -1,blosum62).global(
                                    s_result.seq(), 
                                    t_result.seq()
                                ).score);
                });
        });
    let end = Instant::now();
    let runtime = end - start;
    let mut file = File::create("runtime_rust.txt").unwrap();
    file.write_all(format!("Completed in: {} seconds", runtime.as_secs_f64()).as_bytes()).unwrap();
}


